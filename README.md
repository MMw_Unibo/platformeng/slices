# Slices
In this project we aim to develop an end-to-end platform for the holistic management and orchestration of Cloud Continuum resources, capable of proactive and dynamic (re)configuration to meet service and application requirements. Our approach abstracts hardware and software resources, standardizing their observation and management through tailored Kubernetes-defined control loops. We implemented this concept targeting the lifecycle management of a distributed learning process.

## Project Structure


**Slices**\
── :page_with_curl:[Wiki](https://gitlab.com/groups/MMw_Unibo/platformeng/-/wikis/(mainv2)Platform-installation-+-infrastructure-setup-(vagrant))\
── :computer:[TestBed](https://gitlab.com/MMw_Unibo/platformeng/testbed)\
\
├── [Crossplane Composite Resources](https://gitlab.com/MMw_Unibo/platformeng/crossplane-compositeresources)\
│&emsp;&emsp;├──:open_file_folder:[xTopic](https://gitlab.com/MMw_Unibo/platformeng/crossplane-compositeresources/-/tree/main/xTopic?ref_type=heads)\
│&emsp;&emsp;├──:open_file_folder:[xFL](https://gitlab.com/MMw_Unibo/platformeng/crossplane-compositeresources/-/tree/main/xFL?ref_type=heads)\
│&emsp;&emsp;└──:open_file_folder:[xBroker](https://gitlab.com/MMw_Unibo/platformeng/crossplane-compositeresources/-/tree/main/xFL?ref_type=heads)\
│   \
├── Infrastructure providers\
│&emsp;&emsp;├── :open_file_folder:[Provider FL Client](https://gitlab.com/MMw_Unibo/platformeng/provider-flclient)\
│&emsp;&emsp;├── :open_file_folder:[Topic Provider](https://gitlab.com/MMw_Unibo/platformeng/topic-provider)\
│&emsp;&emsp;├── :open_file_folder:[Provider FederatePipeline](https://gitlab.com/MMw_Unibo/platformeng/provider-federatedpipeline)\
│&emsp;&emsp;└──\
│\
└── Utility providers\
&emsp;&emsp;├── :open_file_folder:[Baremetal-Process Provider](https://gitlab.com/MMw_Unibo/platformeng/baremetal-process-provider)\
&emsp;&emsp;└── :open_file_folder:[Provider Kubernetes](https://github.com/crossplane-contrib/provider-kubernetes)



## Getting started with the Tutorial

Please, refer to the [Wiki](https://gitlab.com/groups/MMw_Unibo/platformeng/-/wikis/(mainv2)Platform-installation-+-infrastructure-setup-(vagrant)) to install the infrastructure on Virtual Box through Vagrant. 

## Introduction to Crossplane

Please, refer to the official [Crossplane Documentation](https://docs.crossplane.io/latest/)

Crossplane is a **control-plane framework**. Control planes manage the infrastructure using Kubernetes constructs. It enables a unified approach to managing both **application** and **infrastructure** resources using Kubernetes-native tools and workflows.

Crossplane makes easy the definition of Kubernetes custom resources (CR), the installation of the resources' controllers, and the installation of all the relative kubenretes resource in order to make the CRs controllable through the Kubernetes APIs. 

### Kubernetes custom resource definition

Kubernetes offers the possibility to define custom resources that contain a state/ knowledge about an external or a virtual resource. A custom resource becames a kind offered by kubernetes that can be created, deleted, updated or obtained through a get request. 

However, the custom resource itself is not mapped inside the Kube-controller manager: the observation and the update of the resource and its representation in the kubernetes knowledge, must be integrated separately, for example adding a pod that contains the control logic of the resource. 

Crossplane allows you to ealsily connect the controller to the resource. 

### Crossplane Provider Template 

Crossplane offers a [provider template](https://github.com/crossplane/provider-template) that automatizes the creation of Custom resource definitions, controllers, and configurations to connect the controller to the resource. 
The result of the make build instruction of the Crossplane Provider template is a package file (pkg) that can be loaded into kubernetes throught a registry or through the crossplane client. On the cluster, it contain all the configurations to install the new managed resource. 

To define a Kubernetes managed resource using the Crossplane provider Template, you need to: 
- define the parameters of the custom resource that can be defined through the Kubernetes API (eventually in a Yaml configuration file)
- define the resource observable state, that can be obtained through the Kubernetes API "GET". 
- define the control logic for the controll loop in the Kubernetes controller: how to **observe** the resource, thus **create** it, eventually **update** or **delete** it. 

### Crossplane Composite Resources 

